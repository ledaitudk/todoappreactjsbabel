import React from 'react';
import '../styles/index.scss';
import InputMessenger from './common/inputMessenger.jsx';
import ShowMessenger from './common/showMessenger.jsx';
export default class App extends React.Component {
    constructor(){
        super();
        this.state = ({listChatMessenger:[]});
        this.dataResult = this.dataResult.bind(this);
    }

    dataResult(chatMessenger){
        var chatMessengerItem = {id:this.state.listChatMessenger.length, text: chatMessenger};
        var listChatMessengerTemp = this.state.listChatMessenger;
        listChatMessengerTemp.push(chatMessengerItem);
        this.setState({listChatMessenger: listChatMessengerTemp});

        console.log(chatMessenger +" App");
        console.log(chatMessengerItem);
    }

    render() {
       return(
           <div className="container">
               <div className="col-md-12 show-messenger">
                   <ShowMessenger listChatMessenger = {this.state.listChatMessenger}/>
               </div>
               <div className="col-md-12 input-messenger">
                   <InputMessenger resultData = {this.dataResult}/>
               </div>
           </div>
       )
    }
}
