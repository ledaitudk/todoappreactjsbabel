import React from 'react';

export default class ShowMessenger extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
        var chatMessenger = this.props.listChatMessenger.map(function (chatMessengerItem) {
           return(
               <div className="chat-messenger-item" key={chatMessengerItem.id}>
                   <p>{chatMessengerItem.text}</p>
               </div>
           )
        });
        return(
            <div className="show-chat-item">
                {chatMessenger}
            </div>
        )
    }
}