import React from 'react';

export default class InputMessenger extends React.Component{
    constructor(props){
        super(props);
        this.state = ({chatMessenger: ''});
        this.handOnchange = this.handOnchange.bind(this);
        this.onBindData = this.onBindData.bind(this);
    }

    handOnchange(event){
        this.setState({chatMessenger:event.target.value});
        console.log(this.state.chatMessenger +" Input");
    }

    onBindData(){
        this.props.resultData(this.state.chatMessenger);
    }

    render(){
        return(
            <div className="row input-page">
                <div className="col-md-10">
                    <input type="text" className="form-control" placeholder="Nhập tin nhắn..." onChange={this.handOnchange}/>
                </div>
                <div className="col-md-2">
                    <button className="btn btn-success" onClick={this.onBindData}>Send Messenger</button>
                </div>
            </div>
        );
    }
}